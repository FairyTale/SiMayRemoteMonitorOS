﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiMay.Core.Packets.Screen
{
    public class ScreenSetQtyPack
    {
        public long Quality { get; set; }
    }
}
