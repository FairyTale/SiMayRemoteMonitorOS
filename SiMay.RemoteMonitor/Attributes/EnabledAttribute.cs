﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMay.RemoteMonitor.Attributes
{
    public class DisableAttribute : Attribute
    {
        public DisableAttribute()
        {
        }
    }
}
