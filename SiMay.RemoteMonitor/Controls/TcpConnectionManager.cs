﻿using SiMay.Core;
using SiMay.Core.PacketModelBinder.Attributes;
using SiMay.Core.PacketModelBinding;
using SiMay.Core.Packets.TcpConnection;
using SiMay.Net.SessionProvider.SessionBased;
using SiMay.RemoteMonitor.Attributes;
using SiMay.RemoteMonitor.ControlSource;
using SiMay.RemoteMonitor.Extensions;
using SiMay.RemoteMonitor.Notify;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SiMay.RemoteMonitor.Controls
{
    /// <summary>
    /// 控制源特性
    /// </summary>
    [ControlSource(90, "Tcp连接管理", "TcpConnectionManagerJob", "", false)]
    public partial class TcpConnectionManager : Form, IControlSource
    {
        MessageAdapter _adapter;
        private string _title = "//Tcp连接管理 #Name#";
        private Dictionary<string, ListViewGroup> _groups = new Dictionary<string, ListViewGroup>();
        private PacketModelBinder<SessionHandler> _handlerBinder = new PacketModelBinder<SessionHandler>();
        /// <summary>
        /// 构造函数必须为(MessageAdapter adapter)，否则将引发异常
        /// </summary>
        /// <param name="adapter"></param>
        public TcpConnectionManager(MessageAdapter adapter)
        {
            _adapter = adapter;
            adapter.OnSessionNotifyPro += Adapter_OnSessionNotifyPro;
            adapter.ResetMsg = this.GetType().GetControlKey();
            _title = _title.Replace("#Name#", adapter.OriginName);

            InitializeComponent();
        }

        private void Adapter_OnSessionNotifyPro(SessionHandler session, SessionNotifyType notify)
        {
            switch (notify)
            {
                case SessionNotifyType.Message:
                    if (_adapter.WindowClosed)
                        return;
                    _handlerBinder.InvokePacketHandler(session, session.CompletedBuffer.GetMessageHead(), this);
                    break;
                case SessionNotifyType.OnReceive:
                    break;
                case SessionNotifyType.ContinueTask:
                    this.Text = _title;

                    break;
                case SessionNotifyType.SessionClosed:
                    this.Text = _title + " [" + _adapter.TipText + "]";
                    break;
                case SessionNotifyType.WindowShow:
                    this.Show();
                    break;
                case SessionNotifyType.WindowClose:

                    _adapter.WindowClosed = true;
                    this.Close();

                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 开始启动
        /// </summary>
        public void Action()
        {
            this.Show();
        }

        [PacketHandler(MessageHead.C_TCP_LIST)]
        public void TcpListHandler(SessionHandler session)
        {
            var pack = session.CompletedBuffer.GetMessageEntity<TcpConnectionPack>();
            lstConnections.Items.Clear();

            foreach (var con in pack.TcpConnections)
            {
                string state = con.State.ToString();

                ListViewItem lvi = new ListViewItem(new[]
                {
                    con.ProcessName,
                    con.LocalAddress,
                    con.LocalPort.ToString(),
                    con.RemoteAddress,
                    con.RemotePort.ToString(),
                    state
                });

                if (!_groups.ContainsKey(state))
                {
                    // create new group if not exists already
                    ListViewGroup g = new ListViewGroup(state, state);
                    lstConnections.Groups.Add(g);
                    _groups.Add(state, g);
                }

                lvi.Group = lstConnections.Groups[state];
                lstConnections.Items.Add(lvi);
            }
        }

        /// <summary>
        /// 关闭窗口前
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TestApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            //及时的通知远程关闭连接并释放资源
            _adapter.SendAsyncMessage(MessageHead.S_GLOBAL_ONCLOSE);

            //手动关闭设为true,否则系统将会重新连接
            _adapter.WindowClosed = true;
        }

        private void TcpConnectionManager_Load(object sender, EventArgs e)
        {
            this.Text = _title;
            _adapter.SendAsyncMessage(MessageHead.S_TCP_GET_LIST);
        }

        private void 刷新列表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _adapter.SendAsyncMessage(MessageHead.S_TCP_GET_LIST);
        }

        private void 关闭连接ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<KillTcpConnectionItem> kills = new List<KillTcpConnectionItem>();
            foreach (ListViewItem lvi in lstConnections.SelectedItems)
            {
                kills.Add(new KillTcpConnectionItem()
                {
                    LocalAddress = lvi.SubItems[1].Text,
                    LocalPort = lvi.SubItems[2].Text,
                    RemoteAddress = lvi.SubItems[3].Text,
                    RemotePort = lvi.SubItems[4].Text
                });
            }

            _adapter.SendAsyncMessage(MessageHead.S_TCP_CLOSE_CHANNEL, new KillTcpConnectionPack()
            {
                Kills = kills.ToArray()
            });
        }
    }
}
